import sys
import os
import json
import glob
import test_db as td

def split_file_string(file_name):
    spl = file_name.split('_')
    competition = spl[0]
    country = ' '.join(spl[1:-1])
    year = ''.join(spl[-1:]).split('.')[0]
    return competition.upper(), country.upper(), year.upper()

def insert_data(event, raceresults):
    i = 0
    files = glob.glob("*.json")
    total = len(files)
    for file in files[:1]:
        with open(file, "r") as json_file[1:]:
            comp, country, year = split_file_string(file)
            print("Current file: " + file)
            print(data["0"])
            if (len(data) > 0):
                raceresults[comp].insert({
                    "Country": [country],
                    "Year": [year]
                })
            i += 1
            print("Processed " + str(i) + "/" + str(total))


def insert_test(comp_name):
    i = 0
    files = glob.glob("*.json")
    total = len(files)
    for file in files:
        comp, country, year = split_file_string(file)
        year,c=td.Year.get_or_create(year=int(year))
        country,c=td.Country.get_or_create(name=country)
        comp,c=td.Competition.get_or_create(name=comp_name,year=year,country=country)
        print(year)
        print(country)
        
        with open(file, "r") as json_file:
            data=json.load(json_file)
            print("Current file: " + file)
            i += 1
            print("Processed " + str(i) + "/" + str(total))

# 4 profession

# 5 citizenship
# 9 state

# 3 name
# 6 age

# 1 division
            
# 0 swim_t
# 2 run_t
# 7 run_d
# 8 bib
# 10 bike_t
# 12 overall_t
# 13 swim_d
# 14 overallRank
# 15 points
# 16 t2
# 17 t1
# 18 divRank


def main(dir_name):
    os.chdir(dir_name + "/JSON")
    local = True
    td.db.connect()
    comp_name,created=td.Competition_Type.get_or_create(name=dir_name)
    print(created)
    print(comp_name)
    insert_test(comp_name)
    td.db.close()
#    insert_data(dir_name, db)
    


if __name__ == '__main__':
    main(sys.argv[1])
