import mysql.connector
from peewee import *
from datetime import date

db=MySQLDatabase('testing',user='db',password='db')

class BaseModel(Model):
    class Meta:
        database=db

class Admin(BaseModel):
    name=CharField(unique=True)

class Year(BaseModel):
    year=IntegerField()
        
class Division(BaseModel):
    division=CharField(unique=True)
        
class Profession(BaseModel):
    prof=CharField(unique=True)

class User(BaseModel):
    name=CharField(unique=True)

class State(BaseModel):
    name=CharField(unique=True)
    
class Citizenship(BaseModel):
    name=CharField(unique=True)
    state=ForeignKeyField(State,backref='citizenships')

class Country(BaseModel):
    name=CharField(unique=True)

class Competitor(BaseModel):
    name=CharField(unique=True)
    citizenship=ForeignKeyField(Citizenship,backref='competitors')
    age=IntegerField()
    prof=ForeignKeyField(Profession,backref='competitors')

class Competition_Type(BaseModel):
    name=CharField(unique=True)
    
class Competition(BaseModel):
    name=ForeignKeyField(Competition_Type,backref='competitions')
    year=ForeignKeyField(Year,backref='competitions')
    country=ForeignKeyField(Country,backref='competitions')

class RaceResult(Model):
    overall_r=IntegerField()
    overall_t=TimeField('%H:%M:%S')
    div_rank=IntegerField(unique=True)
    division=ForeignKeyField(Division,backref='raceresults')
    points=IntegerField()
    
    swim_d=CharField()
    t1=TimeField('%H:%M:%S')
    bike_d=CharField()
    t2=TimeField('%H:%M:%S')
    run_d=CharField()
    bib=IntegerField(unique=True)

    swim_t=FloatField()
    run_t=FloatField()
    bike_t=FloatField()
    
    name=ForeignKeyField(Competitor,backref='raceresults')
    competition=ForeignKeyField(Competition,backref='raceresults')

    class Meta:
        database=db
        indexes={
            (('name','competition'),True)
        }

def create_databases():
    db.connect()
    tables=[
            User,
            Admin,
            Division,
            Profession,
            Year,
            State,
            Citizenship,
            Country,
            Competitor,
            Competition_Type,
            Competition,
            RaceResult
    ]
    db.create_tables(tables)
    db.close()

    if __name__=="__main__":
        create_databases()
