#! /bin/bash

echo "Input zip/bzip/gzip: "
#read fullfile

local=$2
extract=$3

echo "Arguments: $0 $1 $2"

fullfile="Race-Results.zip"
filename=$(basename -- "$fullfile")
filename="${filename%.*}"

if [ $extract == 'e' ];
then
    echo "Exctracting"
    unzip -q -B "$(realpath $fullfile)"
fi

echo "cd into $filename"
cd $filename
for file in *
do
    if [ -d $file ];
    then
	if [ "$file" != *"Ultra"* ];
	then
	    pwd
	    python3 ../database/make_db.py $file $2
	fi
    fi
done

	



   
